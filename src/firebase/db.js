import firebase from "firebase";
import "firebase/firestore";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
var firebaseConfig = {
  apiKey: "AIzaSyBvphecApZr6ILUpayrdyEIn1aeVM30FkU",
  authDomain: "crackers-24536.firebaseapp.com",
  projectId: "crackers-24536",
  storageBucket: "crackers-24536.appspot.com",
  messagingSenderId: "1094703564202",
  appId: "1:1094703564202:web:f3e39199a7fba635f017d3",
  measurementId: "G-YB6V4Z9XZZ",
};
// Initialize Firebase
export const db = firebase.initializeApp(firebaseConfig).firestore();
const { Timestamp, GeoPoint } = firebase.firestore;
export { Timestamp, GeoPoint };
