import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import vuetoast from "./plugins/vuetost";
import router from "./router";
import store from "./store";
import ScrollingAnimation from "../public/directives/scrollingAnimation";
import { firestorePlugin } from "vuefire";

Vue.directive("scrollanimation", ScrollingAnimation);
Vue.use(firestorePlugin);

Vue.config.productionTip = false;

new Vue({
  store,
  vuetify,
  router,
  vuetoast,
  render: (h) => h(App),
}).$mount("#app");
