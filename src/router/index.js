import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";
import About from "../views/About.vue";
import Brand from "../views/Brand.vue";
import videoGallery from "../views/VideoGallery.vue";
import product from "../views/product.vue";
import contact from "../views/contact.vue";
import ProductBox from "../views/ProductBox.vue";
import admin from "../views/admin.vue";
import carousel from "../views/carousel.vue";
// import newData from "../components/newData.vue"
import dbone from "../components/dbone.vue"
import firebase from 'firebase'
import OrderConfirm from '../components/OrderConfirm.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home,
  },
  {
    path: "/Order",
    name: "About",
    component: About,
  },
  {
    path: "/brand",
    name: "Brand",
    component: Brand,
  },
  {
    path: "/videoGallery",
    name: "videoGallery",
    component: videoGallery,
  },
  {
    path: "/product",
    name: "product",
    component: product,
  },
  {
    path: "/contact",
    name: "Contact",
    component: contact,
  },
  {
    path: "/productbox",
    name: "ProductBox",
    component: ProductBox,
  },
  {
    path: "/OrderConfirm",
    name: "OrderConfirm",
    component: OrderConfirm,
  },
  {
    path: "/carousel",
    name: "carousel",
    component: carousel,
  },
  {
    path: "/admin",
    name: "admin",
    component: admin,
  },
  // {
  //   path: "/newData",
  //   name: "newData",
  //   component: newData,
  // },
  {
    path: "/Crackers14121998_BackendBb",
    name: "dbone",
    component: dbone,
    meta: {
      requiresAuth: true,
    },
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});
router.beforeEach((to,from,next) => {
  let currentUser = firebase.auth().currentUser
  if(to.matched.some(route => route.meta.requiresAuth)){
    if(currentUser) return next();

    return next('/admin');
  }

  next();
});

export default router;
