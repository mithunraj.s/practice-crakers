import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  namespaced: true,
  state: {
   usernumber:null,
   orderData:null,
   userName:"",
   userMobile:"",
   userEmail:"",
   userzipCode:"",
   userAddress:""
    // dbView:false
  },
  mutations: {
    setDataBase(state, data) {
      state.Db_Data = data;
      console.log(state.Db_Data);
    },
  },
  getters: {
    final(state) {
      return state.Db_Data;
    },
  },
  actions: {},
});
